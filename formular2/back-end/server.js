const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER,vârstă VARCHAR(3), CNP VARCHAR(20)";
    connection.query(sql, function (err, result) {
        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let vârstă = req.body.vârstă;
    let CNP = req.body.CNP;;
    let error = []
    if (!nume||!prenume||!telefon||!email||!facebook||!tipAbonament||!nrCard||!cvv||!vârstă||!CNP) {
        error.push("Unul sau mai multe campuri nu au fost introduse");
        console.log("Unul sau mai multe campuri nu au fost introduse!");
    }else{
        if(nume.length < 3 || nume.length > 20) {
            console.log("Nume invalid!");
            error.push("Nume invalid!");
        }else if (!nume.match("^[A-Za-z]+$")) {
            console.log("Numele trebuie sa contina doar litere!");
            error.push("Numele trebuie sa contina doar litere!");
          }
          if (prenume.length < 3 || prenume.length > 20) {
            console.log("Prenume invalid!");
            error.push("Prenume invalid!");
          } else if (!prenume.match("^[A-Za-z]+$")) {
            console.log("Prenumele trebuie sa contina doar litere!");
            error.push("Prenumele trebuie sa contina doar litere!");
          }
          if (telefon.length != 10) {
            console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
            error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
          } else if (!telefon.match("^[0-9]+$")) {
            console.log("Numarul de telefon trebuie sa contina doar cifre!");
            error.push("Numarul de telefon trebuie sa contina doar cifre!");
          }
          if (email.match("^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}^")){
          console.log("Email invalid!");
          error.push("Email invalid!");
          }
          if (!facebook.includes("https://")){
          console.log("Link invalid");
          error.push("Link invalid");
          }
          if(nrCard.length !=16){
              console.log("NrCard invalid");
              error.push("NrCard invalid");
          }else if(!nrCard.match("^[0-9]+$^")){
              console.log("Numarul cardului trebuie sa contina doar cifre!");
              error.push("Numarul cardului trebuie sa contina doar cifre!");
          }
          if(cvv.length !=3){
            console.log("cvv invalid");
            error.push("cvv invalid");
        }else if(!nrCard.match("^[0-9]+$^")){
            console.log("cvv trebuie sa contina doar cifre!");
            error.push("cvv trebuie sa contina doar cifre!");
        }
        if(vârstă.length < 1 || vârstă.length >3){
            console.log("Varsta este invalida");
            error.push("Varsta este invalida!");
        }
        else if(!vârstă.match("^[0-9]+$^")){
            console.log("Varsta este invalida!");
            error.push("Varsta este invalida!");
        }
    }
    if (error.length === 0) {
        const sql =
            "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv,vârstă,CNP,sex) VALUES('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','" +facebook +"','" + tipAbonament +"','" + nrCard +"','" +cvv+"','" + vârstă +"','" + CNP +"', '" + sex + "')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }
});